---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: single
paginate: 5 # amount of posts to show
paginate_path: /page:num/
---

#Welcome
To edit this file, open ``index.md``

This is a vanilla [Jekyll](https://jekyllrb.com/) site with some light configuration so it auto-publishes to Gitlab pages.
[More information here](https://gitlab.com/andrewsempere/jekyll-demo).

It is also set up to use the Minimal Mistakes theme. For documentation click "Quick-Start Guide" above. Every aspect of this setup is configurable, but the idea is that by default everything is hidden and you expose the overrides that you need. See the guide to figure out how and what you need to add to make the changes you like.

##Here's how you include an image:  
<img src="{{site.assets_img}}/doggo.jpg">

- Asset server is configured as: ``{{site.assets_img}}``

- You can change this in your `_config.yml` file, don't forget to restart jekyll to pick up those changes. Most changes will refresh automatically, but _config.yml is a special case.

- Notice that curly-brace stuff in the sourcefile? That's an example of [liquid notation](https://jekyllrb.com/docs/liquid/), it's how you get things done in Jekyll.
