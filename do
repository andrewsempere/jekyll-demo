#!/bin/bash
#source .env

# Determine RUNDIR, sensitive to softlinks
# From: https://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  RUNDIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$RUNDIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
export RUNDIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

# Case
case "$1" in
        publish)
			git add .
			git commit -m 'Jekyll Admin Update'
			git push
            ;;

        develop)
			bundle exec jekyll serve --config "_config.yml,_config_dev.yml"
            ;;


        *)
            echo $"Usage: $0 {publish|develop}"
            exit 1
esac
