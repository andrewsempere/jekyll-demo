## Jekyll + S3 + Gitlab
I've jumped on the static web hosting bandwagon and am happy to spread the gospel. This repository is an attempt to streamline it for others. The end result will be a Jekyll site generator that publishes via Gitlab CI and uses S3 for assets. Optionally you can set up SSL via LetsEncrypt as well. It assumes you're on OSX.
[You can see what this site should look like here](https://andrewsempere.gitlab.io/jekyll-demo).

## Why?
Most websites and even blogs are fine being served as static content. This is faster, lighter weight, more secure and less hassle to maintain than, say, a Wordpress server. Gitlab and Github both offer free hosting on static websites, so we can take advantage of that. What about assets like images and videos? S3 is perfect. Amazon is a bit expensive, but competitor Wasabi works great, and if you need to scale you can easily add Cloudflare in front of either of them.

## Initial Setup (OSX)
- Optional: [Homebrew](https://brew.sh/) This is not strictly necessary but you’ll find it useful if you start doing pretty much any dev work at all on your Mac.

- Optional: If your terminal is underutilized and is in the default black and white, you will find life is easier if you add some colors. "Sexy Bash" has a dumb name but it’s a good start: https://github.com/twolfson/sexy-bash-prompt

- Optional: Atom text editor, great general purpose text editor:
https://atom.io/

- **Required**: Make sure you have Ruby and Jekyll installed and working ok on your machine. Tutorial [here via the Jekyll site](https://jekyllrb.com/docs/).


-  **Required**: Set up a [Gitlab](https://gitlab.com) account, and configure it to use SSH.
	- SSH Setup: If you've never set up SSH before on your machine, the idea is to create a "key pair" which allows you to securely identify yourself without carrying around a password. [Here's how to set it up](https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/). Mini SSH opsec: never ever ever share the private files (end in .private or no extension) as this will give unrestricted access to all ssh resources. The .pub file is meant to be shared - not generally a good idea to post it on a public server, but you can freely give it to sites that support ssh.  

	- After you have your Gitlab account and your SSH key: In the upper right of Gitlab,under your icon click “settings” and then on the menubar on the left click “ssh keys” (the first icon that looks like a key). Cut and paste the contents of the `` ~/.ssh/id_rsa.pub`` file into the “key” field. Title can be whatever you want, something like “My Laptop” is fine.


- **Required**: Set up a [Wasabi](https://wasabi.com/) account (or an [Amazon AWS](http://aws.amazon.com)) and make an S3 bucket for your assets, it should be named something like: assets.example.com. Make that bucket publicly available. Edit `_config.yml` to point to your asset bucket.


- Optional: Install [Mountain Duck](https://mountainduck.io/), which lets you access S3 via the finder so you can drag and drop stuff into your assets. [Here's how to set it up with Wasabi](https://wasabi-support.zendesk.com/hc/en-us/articles/115001671012-How-do-I-use-Cyberduck-or-Mountain-Duck-with-Wasabi-), Amazon S3 is similar.

- **Required**: Clone/Fork this repository and set up your own!

## Advanced Setup

### DNS
You probably want a [custom domain name](https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains) instead of the ``https://mynamespace.gitlab.io.`` address, yes?

### SSL
You can configure your site to be served from Gitlab over SSL using a free certificate from LetsEncrypt. This repository includes a ``.gitlab-ci.yml`` set up for this. By design, LE certificates expire frequently. I've set up a [separate tutorial on how to automate renewal on Gitlab](https://andrewsempere.org/blog/2018/11/01/Gitlab-LetsEncrypt.html).

## How to Use
The Jekyll portion of this repository (everything except this readme and the .gitlab-ci.yml file, basically) is a vanilla Jekyll site with the addition of:

- A 'do' script which makes life marginally easier

- The inclusion of the `jekyll_admin` plugin to make editing easier

- An alias to your S3 bucket

- Basic configuration using the [Minimal Mistakes](https://mmistakes.github.io/) theme.

Depending on your needs you might find it easier to make your own fresh Jekyll site or use an existing Jekyll setup and just copy the things you need from this example.

After cloning this repository and setting everything up as above, use terminal to navigate to this directory and:
- `./do develop ` will launch Jekyll locally. You can see your site at: http://127.0.0.1:4000/. You can access a post editor at http://127.0.0.1:4000/admin. You might want to open a second tab in the terminal in the same directory: cmd+T.

- `./do publish` will check in your changes to gitlab. Gitlab will do the rest and your site will be available at: http://EXAMPLE.gitlab.io/jekyll-demo

- Remember that most changes will be picked up automatically, but if you make changes to your `_config.yml` file, you will need to stop and restart 'develop' to see them.
